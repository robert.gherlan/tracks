import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import AccountScreen from './src/screens/AccountScreen';
import SignupScreen from './src/screens/SignupScreen';
import SigninScreen from './src/screens/SigninScreen';
import TrackCreateScreen from './src/screens/TrackCreateScreen';
import TrackDetailScreen from './src/screens/TrackDetailScreen';
import TrackListScreen from './src/screens/TrackListScreen';
import ResolveAuthScreen from './src/screens/ResolveAuthScreen';
import {
  Provider as AuthProvider,
  Context as AuthContext,
} from './src/context/AuthContext';
import { Provider as LocationProvider } from './src/context/LocationContext';
import { navigationRef, isReadyRef } from './src/navigationRef';
import { Provider as TrackProvider } from './src/context/TrackContext';
import { FontAwesome } from '@expo/vector-icons';

const LoginFlow = createStackNavigator();
function LoginFlowScreen() {
  return (
    <LoginFlow.Navigator screenOptions={{ headerShown: false }}>
      <LoginFlow.Screen name="ResolveAuth" component={ResolveAuthScreen} />
      <LoginFlow.Screen name="Signup" component={SignupScreen} />
      <LoginFlow.Screen name="Signin" component={SigninScreen} />
    </LoginFlow.Navigator>
  );
}

const TrackListFlow = createStackNavigator();
function TrackListFlowScreen() {
  return (
    <TrackListFlow.Navigator>
      <TrackListFlow.Screen
        name="TrackList"
        options={{ title: 'Tracks' }}
        component={TrackListScreen}
      />
      <TrackListFlow.Screen name="TrackDetail" component={TrackDetailScreen} />
    </TrackListFlow.Navigator>
  );
}

const MainFlowTab = createBottomTabNavigator();
function MainFlowTabScreen() {
  return (
    <MainFlowTab.Navigator>
      <MainFlowTab.Screen
        name="TrackListFlow"
        component={TrackListFlowScreen}
        options={{
          title: 'Tracks',
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="th-list" size={size} color={color} />
          ),
        }}
      />
      <MainFlowTab.Screen
        name="TrackCreate"
        component={TrackCreateScreen}
        options={{
          title: 'Add Track',
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="plus" size={size} color={color} />
          ),
        }}
      />
      <MainFlowTab.Screen
        name="Account"
        component={AccountScreen}
        options={{
          title: 'Account',
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="gear" size={size} color={color} />
          ),
        }}
      />
    </MainFlowTab.Navigator>
  );
}

function App() {
  const { state } = useContext(AuthContext);
  const { token } = state;

  React.useEffect(() => {
    return () => {
      console.log('Use effect is called.');
      isReadyRef.current = false;
    };
  }, []);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
    >
      {token ? <MainFlowTabScreen /> : <LoginFlowScreen />}
    </NavigationContainer>
  );
}

export default () => {
  return (
    <TrackProvider>
      <LocationProvider>
        <AuthProvider>
          <App />
        </AuthProvider>
      </LocationProvider>
    </TrackProvider>
  );
};

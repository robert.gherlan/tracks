import '../_mockLocation';
import React, { useContext, useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import Map from '../components/Map';
import TrackForm from '../components/TrackForm';
import { Context as LocationContext } from '../context/LocationContext';
import useLocation from '../hooks/useLocation';
import { withNavigationFocus } from '@react-navigation/compat';

const TrackCreateScreen = ({ isFocused }) => {
  const {
    state: { recording },
    addLocation,
  } = useContext(LocationContext);
  const callback = useCallback(
    (location) => {
      addLocation(location, recording);
    },
    [recording]
  );
  const [error] = useLocation(isFocused || recording, callback);

  return (
    <SafeAreaView>
      <Text h2>Track Create Screen</Text>
      <Map />
      {error ? (
        <Text style={styles.error}>Please enable location services.</Text>
      ) : null}

      <TrackForm />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  error: { color: 'red' },
});

export default withNavigationFocus(TrackCreateScreen);

import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const axiosInstance = axios.create({
  baseURL: 'https://6bc3d8eeaaf4.ngrok.io',
});

axiosInstance.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      const newConfig = config;
      newConfig.headers.Authorization = `Bearer ${token}`;
      return newConfig;
    }

    return config;
  },
  (error) => {
    console.error('Axios error.', error);
    return Promise.reject(error);
  }
);

export default axiosInstance;

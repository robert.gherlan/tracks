import createDataContext from './createDataContext';
import trackerApi from '../api/tracker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { navigate } from '../navigationRef';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'add_error_message':
      return { ...state, errorMessage: action.payload };
    case 'signin':
      return { ...state, token: action.payload, errorMessage: '' };
    case 'signout':
      return { errorMessage: '', token: null };
    case 'clear_error_message':
      return { ...state, errorMessage: '' };
    default:
      return state;
  }
};

const signup =
  (dispatch) =>
  async ({ email, password }) => {
    try {
      const response = await trackerApi.post('/signup', { email, password });
      const token = response.data.token;
      await AsyncStorage.setItem('token', token);
      dispatch({ type: 'signin', payload: token });
      navigate('TrackList');
    } catch (error) {
      dispatch({
        type: 'add_error_message',
        payload: 'Something went wrong with sign up.',
      });
    }
  };

const signin =
  (dispatch) =>
  async ({ email, password }) => {
    try {
      const response = await trackerApi.post('/signin', { email, password });
      const token = response.data.token;
      await AsyncStorage.setItem('token', token);
      dispatch({ type: 'signin', payload: token });
      navigate('TrackList');
    } catch (error) {
      dispatch({
        type: 'add_error_message',
        payload: 'Something went wrong with sign in.',
      });
    }
  };

const tryLocalSignin = (dispatch) => async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    dispatch({ type: 'signin', payload: token });
    navigate('TrackList');
  } else {
    navigate('Signup');
  }
};

const clearErrorMessage = (dispatch) => () => {
  dispatch({ type: 'clear_error_message' });
};

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem('token');
  dispatch({ type: 'signout' });
  navigate('loginFlow');
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signup, signin, signout, clearErrorMessage, tryLocalSignin },
  { token: null, errorMessage: '' }
);
